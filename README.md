# Dashboard
<p align="center">
  <img src="./www/img/spinner.svg" width="200" height="200"/><br>
  All your information in the blink of an eye
</p>

# Installation
## With `npm`
1. install a MySQL server distribution
1. update the config in `./config/database.json` accordingly
1. `cd` into the repository
1. run `npm i` to install dependencies
1. run `npm start`

## Using docker-compose
1. `cd` into the repository
1. run `docker-compose up -d`

In either case, you will need to create service applications and provide secrets in `./config/secrets/`.

# Documentation
The project's documentation can be found on the repository [wiki page](../../wikis).

# Authors
@Volifter\
@Alsaka-No-One
